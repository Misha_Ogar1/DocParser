import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import org.jsoup.Jsoup;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String htmlFile = parseFile("code.html");

        Document doc = org.jsoup.Jsoup.parse(htmlFile);

        Elements elements = doc.select("a.ui-product-card-main__wrap");
//        elements.forEach(System.out::println);

        elements.forEach(element -> {
            System.out.println(element.text());
        });
    }

    public static String parseFile(String path) {

        StringBuilder builder = new StringBuilder();
        try{
            List<String> lines = Files.readAllLines(Paths.get(path));
            lines.forEach(line -> builder.append(line + "/n"));
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return builder.toString();
    }

    private static class Jsoup {
    }
}
